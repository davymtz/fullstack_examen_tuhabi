<?php

namespace Tests\Unit;

use App\Models\Amenity;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class AmenityTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_to_amenity_has_properties()
    {
        $amenity = new Amenity();
        $this->assertInstanceOf(Collection::class, $amenity->properties);
    }
}
