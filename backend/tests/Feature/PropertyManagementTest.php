<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Property;
use App\Models\User;
use Tests\TestCase;

class PropertyManagementTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    
    /** @test */
    public function get_all_properties()
    {
        $this->withoutExceptionHandling();
        
        \App\Models\User::factory()->count(1)->make();
        Property::factory()->count(2)->make();

        $response = $this->getJson('http://test.local:8081/api/properties');
        $response->assertOk();

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'created_at',
                    'updated_at',
                    'public_key',
                    'name',
                    'description',
                    'price',
                    'property_type',
                    'operation',
                    'state',
                    'city',
                    'neighborhood',
                    'cp',
                    'street',
                    'latitude',
                    'longitude',
                    'num_bathrooms',
                    'bedrooms',
                    'm2_construction',
                    'parking',
                    'age',
                    'departments',
                    'floor',
                    'user',
                ]
            ]
        ]);
    }

    /** @test */
    public function store_property()
    {
        $this->withExceptionHandling();

        \App\Models\User::factory()->count(1)->create();
        $u = User::first();
        
        $response = $this->postJson('http://test.local:8081/api/properties/createOrEdit', [
            'public_key' => '00'. (String) $this->faker->unique()->randomDigit(),
            'name' => $this->faker->name(),
            'description' => $this->faker->sentence(5),
            'price' => $this->faker->randomFloat(2, 10000, 50000),
            'property_type' => $this->faker->randomElement(['HOUSE','APARTAMENT','TERRAIN','OFFICE','LOCAL']),
            'operation' => $this->faker->randomElement(['SALE','RENT','TRANSFER']),
            'state' => $this->faker->sentence(1),
            'city' => $this->faker->sentence(1),
            'neighborhood' => $this->faker->word(15),
            'cp' => '01234',
            'street' => $this->faker->sentence(1),
            'latitude' => $this->faker->latitude(),
            'longitude' => $this->faker->longitude(),
            'num_bathrooms' => $this->faker->randomDigitNotNull(),
            'bedrooms' => $this->faker->randomDigitNotNull(),
            'm2_construction' => $this->faker->randomDigitNotNull(),
            'parking' => 1,
            'age' => 14,
            'departments' => 0,
            'floor' => 1,
            'images' => [ [ 'src' => ['original' => 'url'] ] ],
            'user' => $u->id,
        ]);

        $response->assertOk();
        $response->assertJsonStructure(["message",]);
    }

    /** @test */
    public function show_property()
    {
        $this->withoutExceptionHandling();
        \App\Models\User::factory()->count(1)->create();
        $user = User::first();
        $property = Property::factory()->create(['user' => $user->id]);

        $response = $this->getJson("http://test.local:8081/api/properties/{$property->id}");
        // $response->dd();
        
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'description',
                'public_key',
                'price',
                'property_type',
                'operation',
                'state',
                'city',
                'neighborhood',
                'cp',
                'street',
                'latitude',
                'longitude',
                'num_bathrooms',
                'bedrooms',
                'm2_construction',
                'parking',
                'age',
                'departments',
                'floor',
                'user',
                'images' => [],
                'amenities' => []
            ]
        ]);
    }

    /** @test */
    public function edit_property() {
        $this->withExceptionHandling();

        \App\Models\User::factory()->create(['id' => 1]);
        Property::factory()->create(['id' => 1]);
        $u = User::first();
        $name_updated = $this->faker->name();

        $response = $this->postJson('http://test.local:8081/api/properties/createOrEdit', [
            'id' => 1,
            'public_key' => '00'. (String) $this->faker->unique()->randomDigit(),
            'name' => $name_updated,
            'description' => $this->faker->sentence(5),
            'price' => $this->faker->randomFloat(2, 10000, 50000),
            'property_type' => $this->faker->randomElement(['HOUSE','APARTAMENT','TERRAIN','OFFICE','LOCAL']),
            'operation' => $this->faker->randomElement(['SALE','RENT','TRANSFER']),
            'state' => $this->faker->sentence(1),
            'city' => $this->faker->sentence(1),
            'neighborhood' => $this->faker->word(15),
            'cp' => '01234',
            'street' => $this->faker->sentence(1),
            'latitude' => $this->faker->latitude(),
            'longitude' => $this->faker->longitude(),
            'num_bathrooms' => $this->faker->randomDigitNotNull(),
            'bedrooms' => $this->faker->randomDigitNotNull(),
            'm2_construction' => $this->faker->randomDigitNotNull(),
            'parking' => 1,
            'age' => 14,
            'departments' => 0,
            'floor' => 1,
            'images' => [ [ 'src' => ['original' => 'url'] ] ],
            'user' => $u->id,
        ]);

        $updated = Property::find(1);

        $response->assertOk();
        $response->assertJsonStructure(["message"]);
        $this->assertEquals($name_updated, $updated->name);
    }
}
