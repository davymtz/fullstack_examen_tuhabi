<?php

namespace App\Http\Controllers;

use App\Http\Traits\PexelsAPI;
use Illuminate\Http\Request;
use App\Models\Property;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PropertyController extends Controller
{

    use PexelsAPI;

    public function index(){
        try {
            // $propeties = Property::all();
            $propeties = Property::with("images")->get();
            return response()->json(["data" => $propeties]);
        } catch (\Exception $e) {
            return response()->json(["error"=> $e->getMessage()],500);
        }
    }
    // public function mvcProperties()
    // {
    //     $propeties = Property::all();
    //     return view('properties',compact('propeties'));
    // }
    public function show($id){
        try {
            $property = Property::Where('id',$id)->with(["images" => function ($i) {
                $i->select(["path","order","property"])->orderBy("order");
            },"amenities" => function ($a) {
                $a->select(["amenity_id","name"]);
            }])->first();

            return response()->json(["data" => $property]);
        } catch (\Exception $e) {
            return response()->json(["error" => $e->getMessage()], 500);
        }
    }

    public function createOrEdit(Request $request) {
        try {
            DB::beginTransaction();
            $images = [];
            $count = 0;
            
            $count_keys = Property::count();
            $post = collect($request->all());
            // Valores seteados
            $post->put("latitude", 19.4114791);
            $post->put("longitude", -99.1762838);
            $post->put("m2_construction", 105);
            // Termina el seteado de variables
            $post->put("public_key", str_pad(($count_keys + 1),4,"0",STR_PAD_LEFT));
            $post->put("user", 1);
            $filtered = $post->except(["amenities", "images", "created_at", "updated_at"]);
            foreach ($post["images"] as $image) {
                $images[] = [
                    "order" => $count += 1,
                    "path" => $image["src"]["original"],
                ];
            }
            if($post->has("id")) {
                $property = Property::Where("id",$post["id"])->first();
                $property->update($filtered->toArray());
            } else {
                $property = Property::create($filtered->toArray());
            }
            $property->amenities()->sync($request->amenities);
            $property->images()->delete();
            $property->images()->createMany($images);
            DB::commit();

            return ($post->has("id"))
                ? response()->json(["message" => "Se actualizó el registro con éxito"]) 
                : response()->json(["message" => "Se insertó el registro con éxito"]);

        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(["errors" => [$e->getLine(), $e->getMessage()]], 500);
        }
    }
}
