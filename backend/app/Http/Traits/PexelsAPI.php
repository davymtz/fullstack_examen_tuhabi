<?php

namespace App\Http\Traits;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

trait PexelsAPI {
    public function pexelSearch($search) {
        $response = Http::withToken(env("API_KEY_PEXELS"))->retry(3)
        ->accept("application/json")->get("https://api.pexels.com/v1/search",["page" => 1,"per_page" => 50,"query" => $search]);
        
        if ($response->successful()) {
            return $response->json();
        } else {
            return response()->json(["message" => "Ha ocurrido un error"],500);
        }
    }

    public function pexelById($id) {
        $response = Http::withToken(env("API_KEY_PEXELS"))->retry(3)
        ->accept("application/json")->get("https://api.pexels.com/v1/photos/{$id}");
        
        if ($response->successful()) {
            return $response->json();
        } else {
            return response()->json(["message" => "Ha ocurrido un error"],500);
        }
    }
}