<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'description',
        'price',
        'property_type',
        'operation',
        'state',
        'city',
        'neighborhood',
        'cp',
        'street',
        'latitude',
        'longitude',
        'num_bathrooms',
        'bedrooms',
        'm2_construction',
        'parking',
        'age',
        'departments',
        'floor',
        'public_key',
        'user',
    ];

    public function user() {
        return $this->belongsTo(User::class,"user","id");
    }

    public function images() {
        return $this->hasMany(PropertyImages::class,"property","id");
    }

    public function amenities() {
        return $this->belongsToMany(Amenity::class,"property_amenities","property_id","amenity_id");
    }
}
