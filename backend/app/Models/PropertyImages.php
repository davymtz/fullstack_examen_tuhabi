<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyImages extends Model
{
    use HasFactory;

    protected $fillable = [
        'order', 'path'
    ];

    public function property() {
        return $this->belongsTo(Property::class,"property","id");
    }
}
