<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_images', function (Blueprint $table) {
            $table->timestamps();
            $table->string('path',300)->nullable();
            $table->integer('order')->nullable();
            $table->unsignedBigInteger('property');
            $table->foreign('property')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_images', function(Blueprint $table){
            $table->dropForeign(['property']);
        });
        Schema::dropIfExists('property_images');
    }
}
