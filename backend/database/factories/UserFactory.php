<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $email = $this->faker->email();
        return [
            'name' => $this->faker->name(),
            'email' => $email,
            'email_verified_at' => $email,
            'password' => $this->faker->password(),
            'remember_token' => Str::random(10), 
        ];
    }
}
