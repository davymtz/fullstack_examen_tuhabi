<?php

namespace Database\Factories;

use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;

class PropertyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     * 
     * @var string
    */
    protected $model = Property::class;

    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'age' => 13,
            'description' => $this->faker->sentence(5),
            'name' => $this->faker->name(),
            'price' => $this->faker->randomFloat(2, 10500, 23450),
            'property_type' => $this->faker->randomElement(['HOUSE','APARTAMENT','TERRAIN','OFFICE','LOCAL']),
            'operation' => $this->faker->randomElement(['SALE','RENT','TRANSFER']),
            'state' => 'CDMX',
            'city' => 'CDMX',
            'neighborhood' => $this->faker->word(10),
            'cp' => '05547',
            'street' => $this->faker->sentence(3),
            'latitude' => $this->faker->latitude(),
            'longitude' => $this->faker->longitude(),
            'num_bathrooms' => $this->faker->randomDigitNotNull(),
            'bedrooms' => $this->faker->randomDigitNotNull(),
            'm2_construction' => 5,
            'parking' => 1,
            'departments' => 0,
            'floor' => $this->faker->randomDigit(),
            'public_key' => "00". (String) $this->faker->unique()->randomDigit(),
            'user' => 1,  
        ];
    }
}
