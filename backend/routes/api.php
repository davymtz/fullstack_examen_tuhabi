<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PropertyController;
use App\Models\Amenity;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('amenities', function (){
    try {
        $amenities = Amenity::select(["id","name"])->get();
        return response()->json([
            "result" => $amenities
        ]);
    } catch(\Exception $e) {
        return response()->json([
            "message" => $e->getMessage()
        ],500);
    }
});
Route::get('properties', 'App\Http\Controllers\PropertyController@index');
Route::get('properties/{property}', 'App\Http\Controllers\PropertyController@show');
Route::post('properties/createOrEdit', 'App\Http\Controllers\PropertyController@createOrEdit');

Route::get('service/pexels/search/{search}', [PropertyController::class, "pexelSearch"]);
Route::get('service/pexels/{id}', [PropertyController::class, "pexelById"]);