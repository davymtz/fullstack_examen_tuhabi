import Axios from "axios";
import {
    ELEMENT,
    GET_ALL_PROPERTIES
} from "./actions";

const API_URL = process.env.REACT_APP_API_URL

export const getAllProperties = () => dispatch => {
    Axios.get(`${API_URL}/properties`).then(resp => {
        return dispatch({
            type: GET_ALL_PROPERTIES,
            properties: resp.data
        })
    })
}

export const showDetailCommit = (id) => dispatch => {
    Axios.get(`${API_URL}/properties/${id}`).then(resp => {
        return dispatch({
            type: ELEMENT,
            detail: resp.data
        })
    })
}

export const getEditProduct = async (id) => {
    const edit = await Axios.get(`${API_URL}/properties/${id}`)
    return edit.data
}

export const getAllAmenities = async () => {
    const allAmenities = await Axios.get(`${API_URL}/amenities`)
    return allAmenities.data
}

export const getImagesPexels = async(payload = 'houses') => {
    const imagesPexels = await Axios.get(`${API_URL}/service/pexels/search/${payload}`)
    return imagesPexels.data
}

export const getProperty = (id) => {
    const resp = Axios.get(`${API_URL}/properties/${id}`)
    return resp
}

export const createOrUpdateProduct = async (payload) => {
    console.log(payload)
    const res = await Axios.post(`${API_URL}/properties/createOrEdit`, payload)
    return res
}
