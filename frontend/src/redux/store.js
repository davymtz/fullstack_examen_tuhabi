import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { propertyReducer, detailReducer } from "./reducers";

export default createStore(
  combineReducers({ propertyReducer, detailReducer }),
  composeWithDevTools(applyMiddleware(thunk))
);
