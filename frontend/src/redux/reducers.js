import {
  GET_ALL_PROPERTIES,
  ELEMENT
} from "./actions";

export const propertyReducer = (state = {}, action) => {
  if (action.type === GET_ALL_PROPERTIES) {
    return {
      ...state,
      properties: action.properties,
    };
  }
  return state
};

export const detailReducer = (state = {}, action) => {
  if (action.type === ELEMENT) {
    return {
      ...state,
      detail: action.detail,
    };
  }
  return state
}
