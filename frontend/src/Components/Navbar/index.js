import { IconButton, Tooltip } from "@mui/material";
import AddCircleSharpIcon from '@mui/icons-material/AddCircleSharp';
import React, { PureComponent } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";

const { Toggle, Collapse, Brand } = Navbar;
const NavLink = Nav.Link;

class NavigationBar extends PureComponent {

  closeSession = () => {
    localStorage.clear()
    window.location = "/"
  }

  render() {

    return (
      <Navbar collapseOnSelect sticky="top" expand="sm" bg="dark" variant="dark">
        <Container>
          <Brand><NavLink href="/">Habi</NavLink> </Brand>
          <Toggle aria-controls="responsive-navbar-nav" />
          <Collapse id="responsive-navbar-nav" className="justify-content-start">
            <Tooltip title="Agregar propiedad">
              <IconButton color="success" aria-label="add property" size="large" href="/property/add">
                <AddCircleSharpIcon />
              </IconButton>
            </Tooltip>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

export default NavigationBar;
