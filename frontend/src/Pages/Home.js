import React, { useEffect } from "react";
import { Col, Row, Container } from "react-bootstrap";
import NavigationBar from "../Components/Navbar";
import { connect } from "react-redux";

import store from "../redux/store";
import { getAllProperties } from "../redux/actionCreators";
import { Link } from "react-router-dom";

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import VisibilityIcon from '@mui/icons-material/Visibility';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { Tooltip } from "@mui/material";

const HomePage = ({ properties }) => {
  useEffect(() => {
    store.dispatch(getAllProperties());
  }, []);

  return (
    <div>
      <NavigationBar />
      <Container fluid>
        <div>
          <div>
            <span>Mapa...</span>
          </div>
          <Container>
            {properties ? (
              <Row md={3} sm={2} xs={1}>
                {properties.map((property) => (
                  <Col key={property.id}>
                    <Card sx={{ maxWidth: 245  }}>
                      <CardMedia component="img" height="180" image={property.images.length > 0 ? property.images[0].path : ""} alt="imagen" />
                      <CardContent>
                        <Typography gutterBottom variant="h6" component="div">{property.name}</Typography>
                        <Typography variant="body2" color="text.secondary">
                          {property.neighborhood} - {property.city}, CP {property.cp}, {property.state}
                        </Typography>
                      </CardContent>
                      <CardActions>
                        <Tooltip title="view detail">
                          <IconButton color="secondary" aria-label="show property" component="span" size="small">
                            <Link to={"/property/"+property.id}>
                              <VisibilityIcon />
                            </Link>
                          </IconButton>
                        </Tooltip>
                        <Tooltip title="edit">
                          <IconButton color="primary" aria-label="edit property" component="span" size="small">
                            <Link to={`/property/${property.id}/edit`}>
                              <EditIcon/>
                            </Link>
                          </IconButton>
                        </Tooltip>
                        <Tooltip title="delete">
                          <IconButton color="error" aria-label="delete property" component="span" size="small">
                            <DeleteIcon />
                          </IconButton>
                        </Tooltip>
                      </CardActions>
                    </Card>
                  </Col>
                ))}
              </Row>
            ) : (
             <div>No properties :(</div>
            )}
          </Container>
        </div>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  properties: state.propertyReducer.properties,
});

export default connect(mapStateToProps, {})(HomePage);
