import React, { useEffect, useState } from "react"
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap"
import { Box, IconButton, ImageList, ImageListItem, ImageListItemBar } from '@mui/material'
import { CheckCircle } from '@mui/icons-material';
import { useNavigate, useParams } from "react-router-dom"
import { getAllAmenities, getImagesPexels, getEditProduct, createOrUpdateProduct } from "../redux/actionCreators"

const AddPropertyForm = () => {
  const params = useParams()
  const navig = useNavigate()
  const [product, setProduct] = useState({})
  const [amenities, setAmenities] = useState([])
  const [pexelService, setPexelService] = useState({ photos: [] })
  const [imageSelected, setImageselected] = useState([])
  // const [validationError, setValidationError] = useState({})

  useEffect(() => {
    getAllAmenities().then(a => setAmenities(a.result))
    getImagesPexels().then(images => setPexelService(images))
    // Get data edit if method edit
    if (Object.entries(params).length > 0) {
      getProductData()
    }
    return () => {
      console.log("Unmount API's")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
  useEffect(() => {
    if (product.images !== undefined && pexelService.photos.length > 0) {
      const $selected = product.images.map((item) => {
        return pexelService.photos.find((image) => item.path === image.src.original)
      }).filter(a => a !== undefined);
      setImageselected($selected)
    }
  }, [product, pexelService])

  async function getProductData() {
    await getEditProduct(params.id).then((edit) => {
      setProduct(edit)
      setProduct(product => ({ ...product, amenities: edit.amenities.map(item => item.amenity_id.toString()) }))
    });
  }

  const handleImages = (item) => {
    if (imageSelected.includes(item)) {
      const pos = imageSelected.indexOf(item);
      setImageselected([...imageSelected.slice(0,pos), ...imageSelected.slice(pos+1)])
    } else {
      setImageselected([...imageSelected, item])
    }
  }
  
  const submitHandler = async (e) => {
    e.preventDefault()
    // setProduct({ ...product, images: imageSelected })
    product.images = Object.assign(product.images, imageSelected) // Temporal :S
    await createOrUpdateProduct(product).then(({data}) => {
      alert(data.message)
      navig("/")
    }).catch((err) => {
      console.log(err);
    })
  }
  // console.log(product);

  return (
    <Container fluid>
      <Row>
        <Col>
          <Card>
            <Card.Body>
              <Card.Title>Agregar propiedad</Card.Title>
              <hr />
              <div className="form-wrapper">
                {/* {Object.keys(validationError).length > 0 && (
                  <Row>
                    <Col>
                    <Alert variant="danger">
                      <Alert.Heading>Hubo un errorcito!</Alert.Heading>
                      <p>
                        <ul className="mb-0">
                        {Object.entries(validationError).map(([key, value]) => (
                          <li key={key}>{value}</li>
                        ))}
                        </ul>
                      </p>
                    </Alert>
                    </Col>
                  </Row>
                )} */}
                <Form onSubmit={submitHandler}>
                  <Row>
                    <Form.Group as={Col} className="mb-3" controlId="name">
                      <Form.Label>Nombre</Form.Label>
                      <Form.Control type="text" name="name" placeholder="Nombre" value={product.name ?? ''} required
                      onChange={(e) => setProduct({ ...product, name: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="description">
                      <Form.Label>Descripción</Form.Label>
                      <Form.Control type="text" placeholder="Descripción" value={product.description ?? ''} required
                      onChange={(e) => setProduct({ ...product, description: e.target.value }) } />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} className="mb-3" controlId="price">
                      <Form.Label>Precio</Form.Label>
                      <Form.Control type="number" placeholder="Precio" value={product.price ?? 0} required
                      onChange={(e) => setProduct({ ...product, price: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="property_type">
                      <Form.Label>Tipo de propiedad</Form.Label>
                      <Form.Select aria-label="Default select example" value={product.property_type ?? ''} required
                      onChange={(e) => setProduct({ ...product, property_type: e.target.value }) }>
                        <option>Seleccione una opción</option>
                        <option value="HOUSE">HOUSE</option>
                        <option value="APARTAMENT">APARTAMENT</option>
                        <option value="TERRAIN">TERRAIN</option>
                        <option value="OFFICE">OFFICE</option>
                        <option value="LOCAL">LOCAL</option>
                      </Form.Select>
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="operation">
                      <Form.Label>Tipo de operación</Form.Label>
                      <Form.Select aria-label="Default select example" value={product.operation ?? ''} required
                      onChange={(e) => setProduct({ ...product, operation: e.target.value }) } >
                        <option>Seleccione una opción</option>
                        <option value="SALE">SALE</option>
                        <option value="RENT">RENT</option>
                        <option value="TRANSFER">TRANSFER</option>
                      </Form.Select>
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="state">
                      <Form.Label>Estado</Form.Label>
                      <Form.Control type="text" placeholder="Estado" value={product.state ?? ''} required
                      onChange={(e) => setProduct({ ...product, state: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="city">
                      <Form.Label>Ciudad</Form.Label>
                      <Form.Control type="text" placeholder="Ciudad" value={product.city ?? ''}
                      onChange={(e) => setProduct({ ...product, city: e.target.value }) } />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} className="mb-3" controlId="neighborhood">
                      <Form.Label>Vecindario</Form.Label>
                      <Form.Control type="text" placeholder="Vecindario" value={product.neighborhood ?? ''} required
                      onChange={(e) => setProduct({ ...product, neighborhood: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="cp">
                      <Form.Label>CP</Form.Label>
                      <Form.Control type="text" placeholder="Código Postal" value={product.cp ?? ''} required
                      onChange={(e) => setProduct({ ...product, cp: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="street">
                      <Form.Label>Calle</Form.Label>
                      <Form.Control type="text" placeholder="Calle" value={product.street ?? ''} required
                      onChange={(e) => setProduct({ ...product, street: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="num_bathrooms">
                      <Form.Label>Número de baños</Form.Label>
                      <Form.Control type="number" placeholder="Número de baños" value={product.num_bathrooms ?? ''} required
                      onChange={(e) => setProduct({ ...product, num_bathrooms: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="bedrooms">
                      <Form.Label>Número de dormitorios</Form.Label>
                      <Form.Control type="number" placeholder="Número de dormitorios" value={product.bedrooms ?? ''} required
                      onChange={(e) => setProduct({ ...product, bedrooms: e.target.value }) } />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} className="mb-3" controlId="parking">
                      <Form.Label>Número de estacionamientos</Form.Label>
                      <Form.Control type="number" placeholder="Número de estacionamientos" value={product.parking ?? ''} required
                      onChange={(e) => setProduct({ ...product, parking: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="age">
                      <Form.Label>Antigüedad</Form.Label>
                      <Form.Control type="number" placeholder="Antigüedad" value={product.age ?? ''} required
                      onChange={(e) => setProduct({ ...product, age: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="departments">
                      <Form.Label>Departamentos</Form.Label>
                      <Form.Control type="number" placeholder="Departamentos" value={product.departments ?? ''} required
                      onChange={(e) => setProduct({ ...product, departments: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="floor">
                      <Form.Label>Piso</Form.Label>
                      <Form.Control type="number" placeholder="Piso" value={product.floor ?? ''} required
                      onChange={(e) => setProduct({ ...product, floor: e.target.value }) } />
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="amenities">
                      <Form.Label>Amenidades</Form.Label>
                      <Form.Select value={product.amenities ?? []} multiple required
                      onChange={(e) => setProduct({ ...product, amenities: [].slice.call(e.target.selectedOptions).map(item => item.value) }) }>
                        {amenities.map((amenity) => (
                          <option key={amenity.id} value={amenity.id}>{amenity.name}</option>
                        ))}
                      </Form.Select>
                    </Form.Group>
                  </Row>
                  <hr />
                  <Row>
                    <Col>
                      <Box sx={{ width: 'auto', height: 450, overflowY: 'scroll' }}>
                        <ImageList variant="masonry" cols={6} gap={8}>
                          {pexelService.photos.map((item) => (
                            <ImageListItem key={item.id}>
                              <img
                              src={`${item.src.original}?w=496&fit=crop&auto=format`}
                              srcSet={`${item.src.original}?w=496&fit=crop&auto=format&dpr=2 2x`}
                              alt={item.alt}
                              loading="lazy" />
                              <ImageListItemBar
                              actionIcon={
                                <IconButton
                                sx={{ color: (imageSelected.includes(item)) ? 'green' : 'rgba(255,255,255,0.54)' }}
                                aria-label="checked button"
                                onClick={() => handleImages(item)}>
                                  <CheckCircle />
                                </IconButton>
                              } />
                            </ImageListItem>
                          ))}
                        </ImageList>
                      </Box>
                    </Col>
                  </Row>
                  <hr />
                  <Button variant="primary" className="mb-3" type="submit">
                    {Object.prototype.hasOwnProperty.call(params,"id") ? "Actualizar" : "Agregar"}
                  </Button>
                  <Button variant="default" className="mb-3" href="/">Cancelar</Button>
                </Form>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default AddPropertyForm;