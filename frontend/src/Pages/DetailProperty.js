import React, { useEffect } from "react";
import {
  Button,
  Card,
  Carousel,
  Col,
  Container,
  Nav,
  Navbar,
  Row,
} from "react-bootstrap";
import { connect } from "react-redux";
import store from "../redux/store";
import { showDetailCommit } from "../redux/actionCreators";
import { useParams } from "react-router-dom";

const { Toggle, Collapse, Brand } = Navbar;
const NavLink = Nav.Link;

const ShowDetail = ({ detail }) => {
  const { id } = useParams()
  useEffect(() => {
    if (id) {
      store.dispatch(showDetailCommit(id));
    }
  }, [id]);

  return (
    <div>
      <Navbar collapseOnSelect sticky="top" expand="sm" bg="dark" variant="dark">
        <Container>
          <Brand><NavLink href="/">Habi</NavLink></Brand>
          <Toggle aria-controls="responsive-navbar-nav" />
          <Collapse id="responsive-navbar-nav" className="justify-content-end"></Collapse>
        </Container>
      </Navbar>
      <Container>
        {detail ? (
            <Card style={{ width: '45rem' }} className="mb-2">
              <Card.Body>
                <Card.Title>
                  <Row>
                    <Col>{detail.name}</Col>
                    <Col>
                      $ {detail.price}
                    </Col>
                  </Row>
                </Card.Title>
                <Card.Subtitle style={{ padding: '0.8em 0' }}>
                  Calle {detail.street}, Col. {detail.neighborhood}, {detail.city}, {detail.state}
                </Card.Subtitle>
                <Card.Subtitle style={{ padding: '0.8em 0' }}>
                  <strong>Detalles del Inmueble</strong>
                </Card.Subtitle>
                <React.Fragment>
                  <Row md={3} sm={2} xs={1} style={{ padding: '0 0' }}>
                    <Col xs lg="auto">CP: {detail.cp}</Col>
                    <Col xs lg="auto">Piso: {detail.floor}</Col>
                    <Col xs lg="auto">Número de cuartos: {detail.bedrooms}</Col>
                    <Col xs lg="auto">Número de baños: {detail.num_bathrooms}</Col>
                    <Col xs lg="auto">Número de baños: {detail.num_bathrooms}</Col>
                    <Col xs lg="auto">Número de estacionamientos: {detail.parking}</Col>
                  </Row>
                  <hr />
                  <Carousel fade>
                    {detail.images.map((image) => (
                      <Carousel.Item key={image.order}>
                        <img className="d-block w-100" src={image.path} alt="imagen_carousel"/>
                      </Carousel.Item>
                    ))}
                  </Carousel>
                  <hr />
                </React.Fragment>
                <Card.Subtitle><strong>Características del Inmueble:</strong></Card.Subtitle>
                <Row md={3} xs={2} className="g-1">
                  {detail.amenities.map((amenity) => (
                    <Col xs="auto" key={amenity.amenity_id}>{amenity.name}</Col>
                  ))}
                </Row>
                <hr />
                <Button variant="secondary" href="/">Atrás</Button>
              </Card.Body>
            </Card>
          ) : (
          <div></div>
        )}
      </Container>
    </div>
  );
}

const showStateDetail = (state) => ({
  detail: state.detailReducer.detail,
})

export default connect(showStateDetail, {})(ShowDetail);