import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/Home";
import ShowDetail from "./Pages/DetailProperty"
import Page404 from "./Pages/Page404";
import AddPropertyForm from "./Pages/AddProperty";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<HomePage/>} path="/" />
        <Route element={<AddPropertyForm/>} path="/property/add" />
        <Route element={<ShowDetail/>} path="/property/:id" />
        <Route element={<AddPropertyForm/>} path="/property/:id/edit" />
        <Route path="*" element={<Page404 />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
